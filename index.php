<?php

require_once 'lib/Router.php';

//Create a new router
$myRouter = new Router();

//Add a Homepage route as a closure
$myRouter->add_route('/', function(){
    echo 'Homepage';
});

//Add another route as a closure
$myRouter->add_route('/greetings', function(){
    echo 'Howdy mate';
});

//Add a route as a callback function
$myRouter->add_route('/callback', 'myFunction');

//Callback function handler
function myFunction(){
    echo "This is a callback function named '" .  __FUNCTION__ ."'";
}

//Execute the router
$myRouter->execute();

